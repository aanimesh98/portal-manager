package com.hcl.dao;

import java.util.List;
import com.hcl.model.Admin;
import com.hcl.model.UserInfo;

public interface IAdminDao {
	
  public int adminAuthentication(Admin admin);
  
  public List<UserInfo> viewAllUser();
  
  public int addUser(UserInfo info);
  
  public int editName(UserInfo info);
  
public int editPasswordFullName(UserInfo info);

public int editPasswordFullNameEmailName(UserInfo info);
  
public int removeUser(UserInfo info);
}


