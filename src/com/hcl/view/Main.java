package com.hcl.view;

import java.util.List;

import java.util.Scanner;
import com.hcl.controller.AdminController;
import com.hcl.model.UserInfo;

public class Main {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter user id");
		String uid = sc.nextLine();
		System.out.println("Enter password");
		String password = sc.nextLine();
		AdminController controller = new AdminController();
		int result = 0;
		result = controller.adminAuthentication(uid, password);
		if (result > 0) {
			// System.out.println("8");
			System.out.println("Hi! " + uid + ", Welcome to Admin Page");
			int cont = 0;
			do {
				System.out.println("1.Add User 2.Remove User 3.Edit User 4.View all User");
				int option = sc.nextInt();
				switch (option) {
				case 1:
					System.out.println("Enter User Id");
					int Uid = sc.nextInt();
					sc.nextLine();
					System.out.println("Enter User Name");
					String uname = sc.nextLine();
					System.out.println("Enter User Password");
					String Password = sc.nextLine();
					System.out.println("Enter User Fullname");
					String fullname = sc.nextLine();
					System.out.println("Enter User Email");
					String email = sc.nextLine();
					result = controller.addUser(Uid, uname, password, fullname, email);
					if (result > 0) {
						System.out.println( Uid + "  Added Successfully!!!");
					}
					break;
				case 2:
					System.out.println("Enter User Id for remove from DB");
					Uid = sc.nextInt();
					result=controller.removeUser(Uid);
					System.out.println((result > 0) ? Uid + " Removed Successfully ": " Remove was not Successfull ");
					break;
				case 3:
					System.out.println("1] Edit Name 2]Edit Password,FullName 3]Edit Password,FullName,Email,Name");
					option = sc.nextInt();
					if (option == 1) {

						System.out.println("Enter User Id");
						int uid1 = sc.nextInt();
						sc.nextLine();
						System.out.println("Enter User Name");
						String uname2 = sc.nextLine();
						result = controller.editName(uname2, uid1);
						System.out.println((result > 0) ? uid + " Updated Successfully " : " Update was not Successfull ");

					} else if (option == 2) {

						System.out.println("Enter User Id");
						int uid2 = sc.nextInt();
						sc.nextLine();
						System.out.println("Enter User Password");
						Password = sc.nextLine();
						System.out.println("Enter User FullName");
						fullname = sc.nextLine();
						result = controller.editPasswordFullName(Password, fullname, uid2);
						System.out.println((result > 0) ? uid + " Updated Successfully " : " Update was not Successfull ");

					} else if (option == 3) {

						System.out.println("Enter User Id");
						int uid3 = sc.nextInt();

						sc.nextLine();
						System.out.println("Enter User Password");
						Password = sc.nextLine();
						System.out.println("Enter User FullName");
						fullname = sc.nextLine();
						System.out.println("Enter User Email");
						email = sc.nextLine();
						System.out.println("Enter User Name");
						uname = sc.nextLine();
						result = controller.editPasswordFullNameEmailName(Password, fullname, email, uname, uid3);
						System.out.println((result > 0) ? uid + " Updated Successfully " : " Update was not Successfull ");

					} else {
						System.out.println("Invalid Edit Option");
					}
					break;
				case 4:
					List<UserInfo> list = controller.viewAllUser();
					if (list.size() > 0) {
						System.out.println("uid,uname,password,fullname,email");
						for (UserInfo u : list) {
							System.out.println(u.getUid() + "," + u.getUname() + "," + u.getPassword() + ","
									+ u.getFullname() + "," + u.getEmail());
						}
					} else {
						System.out.println("No Records Found");
					}
					break;
				default:
					System.out.println("Invalid selection");
				}
				System.out.println("Would You Like To Continue Press 1");
				cont = sc.nextInt();
			} while (cont == 1);
		} else {
			// System.out.println("8");
			System.out.println("User id or Password Incorrect");
		}
		System.out.println("Done!!!");
		sc.close();
	}
}
