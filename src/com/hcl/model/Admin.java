package com.hcl.model;

public class Admin {

	 private String uid;
	 private String password;
	 
	public Admin(String uid, String password) {
		super();
		this.uid = uid;
		this.password = password;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

}

