package com.hcl.model;

public class UserInfo {

private Integer uid;
private String uname;
private String password;
private String fullname;
private String email;
public UserInfo() {
	
	// TODO Auto-generated constructor stub
}

public UserInfo(Integer uid, String uname, String password, String fullname, String email) {
	super();
	this.uid = uid;
	this.uname = uname;
	this.password = password;
	this.fullname = fullname;
	this.email = email;
}

public Integer getUid() {
	return uid;
}

public void setUid(Integer uid) {
	this.uid = uid;
}

public String getUname() {
	return uname;
}

public void setUname(String uname) {
	this.uname = uname;
}

public String getPassword() {
	return password;
}

public void setPassword(String password) {
	this.password = password;
}

public String getFullname() {
	return fullname;
}

public void setFullname(String fullname) {
	this.fullname = fullname;
}

public String getEmail() {
	return email;
}

public void setEmail(String email) {
	this.email = email;
}




}
